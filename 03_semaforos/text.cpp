#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#define MAX_I  1000

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
*spell ( void *text ) {
    int i=0;
    while ( i++ < MAX_I ) {
        printf ( "%i.-\t%s\n", i, (char*) text );
        usleep(1000);
    }
    return 0;
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Texto  > > > ' " );

    const char* text_one = "PRIMERO ----------";
    const char* text_two = "---------- SEGUNDO ";

    pthread_t thread_one, thread_two;


    // Crea los hilos a ejecutar
    pthread_create( &thread_one, NULL, &spell, (void*) text_one );
    pthread_create( &thread_two, NULL, &spell, (void*) text_two );

    // Ejecuta los hilos
    pthread_join ( thread_one, NULL );
    pthread_join ( thread_two, NULL );

    /*
     * El orden de ejecución de cada hilo
     * lo decide el SSTE (Sistema de Soporte en Tiempo de Ejecucion)
     * Por este motivo hacemos uso de semaforos/mutex
     */

    return EXIT_SUCCESS;
}
