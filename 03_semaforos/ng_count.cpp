#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <pthread.h>

#define MAX 100000
int     count = 0;
sem_t   semaphore;   // Variable tipo semaforo global

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
*add (void *arg) {

    for ( int i=0; i<MAX; i++ ) {
        sem_wait ( &semaphore );    // WAIT
        count++;                    // Seccion critica, no es atomica
        sem_post ( &semaphore );    // SIGNAL
    }

    return EXIT_SUCCESS;
}

void
*sub (void *arg) {

    for ( int i=0; i<MAX; i++ ) {
        sem_wait ( &semaphore );
        count--;
        sem_post ( &semaphore );
    }

    return EXIT_SUCCESS;
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Count  > > > ' " );

    pthread_t thread_1, thread_2;                      // Crear variables del tipo pthread_t.

    sem_init ( &semaphore, 0, 1 );                     // Iniciamos el semaforo con un valor de 1, es decir que solo un hilo
                                                       // puede ejecutar la sección critica.

    pthread_create ( &thread_1, NULL, add, NULL );     // Ejecuta los dos hilos, cada uno en una función diferente.
    pthread_create ( &thread_2, NULL, sub, NULL );     // Sin argumentos para las funciones

    pthread_join ( thread_1, NULL );                   // Espera a que termine la ejecucion de cada hilo
    pthread_join ( thread_2, NULL );

    printf( "Valor de count: %i\n\n", count );

    return EXIT_SUCCESS;
}
