#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define MAX 100000
int count = 0;

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
*add (void *arg) {
    for (int i=0; i<MAX; i++)
        count++;

    return EXIT_SUCCESS;
}

void
*sub (void *arg) {
    for (int i=0; i<MAX; i++)
        count--;

    return EXIT_SUCCESS;
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Count  > > > ' " );

    pthread_t thread_1, thread_2;                     // Crear variables del tipo pthread_t

    pthread_create ( &thread_1, NULL, add, NULL);     // Ejecuta los dos hilos, cada uno en una función diferente
    pthread_create ( &thread_2, NULL, sub, NULL);     // si todo fuese bien el counter deberia ser 0

    printf( "Valor de count: %i\n\n", count );

    return EXIT_SUCCESS;
}
