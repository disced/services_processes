#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  PID  > > > ' " );

    pid_t pid, ppid;

    system ("ps");

    pid  = getpid();   // Get current process id
    ppid = getppid();  // Get parent process id

    printf ( "\n\nCurrent Process Id is: %i\nThe Parent Process Id is: %i\n\n", pid, ppid );

    while (true) {
        printf ( "\n\nCurrent Process Id is: %i\nThe Parent Process Id is: %i\n\n", pid, ppid );
        sleep (2);
    }

    return EXIT_SUCCESS;
}
