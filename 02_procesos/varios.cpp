#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

enum TProcess{ CHILD, PARENT };

const char *process_type[] = { "CHILD", "PARENT" };
void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Varios  > > > ' " );

    pid_t child_pid, pid;
    int shared;

    printf ( "Parent of current PID:\t%i\nCurrent PID:\t\t%i\n\n",
            getppid(),
            getpid() );


    child_pid = fork(); // Create a child process equal to parent

    while (true) {

        if ( child_pid == 0 )
            shared = CHILD, pid = getpid();

        else if ( child_pid > 0 )
            shared = PARENT, pid = getpid();

        printf ( "\t%s -- PID: %i\n\n", process_type[shared], pid );
        sleep(3);
    }

    return EXIT_SUCCESS;
}
