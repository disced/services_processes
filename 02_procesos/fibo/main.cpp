#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
print_usage ( FILE *file ) {
    fprintf ( file, "$ ./fibo first second max\n\n\t$ ./fibo 1 1 10\n" );
}

void
print_fibo ( unsigned res ) {
    printf ( "%i\t\tPID: %i PPID: %i\n", res, getpid(), getppid() );
}

void
fibo ( unsigned long first, unsigned long second, unsigned size, unsigned full_size ) {
    pid_t child = fork();      // Create child process 
    unsigned long res;

    if ( size == full_size )   // Verify current size and full size
        exit;
    else {
        if ( child > 0 )       // Finalize the parent process
            exit;
        else if ( child == 0 ) {
            res = first + second;

            print_fibo ( res );
            fibo ( second, res, size, ++full_size );
        }
    }

}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Fibo  > > > ' " );
    unsigned first, second;

    if ( argc < 4 )
        print_usage( stdout );

    else {
        first  = atoi (argv[1]);
        second = atoi (argv[2]);

        print_fibo ( first  );
        print_fibo ( second );

        fibo ( first, second, atoi( argv[3] ), 0 );

    }

    return EXIT_SUCCESS;
}
