/*
 * =====================================================================================
 *       Filename:  commands.h
 *    Description:
 *
 *        Created:  20/10/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */


#ifndef __COMMANDS_H__
#define __COMMANDS_H__

#ifdef __cplusplus
extern "C"
{
#endif

void spawn ( const char*, char** );

#ifdef __cplusplus
}
#endif

#endif
