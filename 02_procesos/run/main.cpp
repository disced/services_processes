#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "commands.h"

const char *commands[] = { "vim", "firefox", "gedit", "nautilus", NULL };

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Run > > > ' " );

    unsigned opc, i=0;


    printf ( "Select Option:\n" );
    while ( commands[i] != NULL ) {
        printf ( "\t%i.- %s\n", (i+1), commands[i] );
        i++;
    }

    scanf ( " %i", &opc );
    spawn ( commands[--opc], NULL );

    return EXIT_SUCCESS;
}
