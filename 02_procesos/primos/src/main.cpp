#include "stack.h"
#include "process.h"

#include <stdlib.h>
#include <string.h>

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Primos > > > ' " );

    process();

    return EXIT_SUCCESS;
}
