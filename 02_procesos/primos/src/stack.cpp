#include "stack.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "files.h"

unsigned *stack = NULL;
unsigned *first_position = stack;
int head_stack  = 0;

void pushStack( unsigned number )
{
    stack = (unsigned *)realloc(stack, (head_stack + 1) * sizeof(unsigned));
    stack[head_stack] = number;
    head_stack++;
}

void writeStackFile( int fd )
{
    char *buff;
    const char *format_out = "%i.- %i\n";
    for (int i=0; i<head_stack; i++)
    {
        asprintf( &buff, format_out, i+1, stack[i] );
        write_file( fd, buff, strlen(buff) );
    }
    free( buff );
    deleteStack();
}

void deleteStack()
{
    stack = first_position;
    for (int i=0; i<head_stack; i++)
        free( stack + i );
}

void primeNumber ()
{
    // Calcular los primos entre 4 - 1.000.000 tarda 1,10min~
    unsigned number = 4;
    bool prime;
    while (true)
    {
        if ( (number % 2) == 0 || (number % 3) == 0 || (number % 5) == 0 )
        {
            ++number;
            prime = false;
        } else
        {
            for ( unsigned i=2; i<=(number/2); i++ )
            {
                if ( ( number % i ) >= 1 )
                    prime = true;
                else
                {
                    prime = false;
                    number++;
                    break;
                }
            }
        }
        if ( prime )
        {
            pushStack( number++ );
        }
        //usleep(1000);
    }
}

