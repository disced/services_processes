#include "thread_op.h"
#include <stdlib.h>
#include <string.h>

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' <  Thread X-O  >' " );

    pthread_t thread1, thread2;

    // Creates 2 structs
    TThread struct_thread1, struct_thread2;

    // Define attributes of structs
    struct_thread1.param = TH1_CHAR;     // Thread One Attributes
    struct_thread1.count = TH1_COUNT;

    struct_thread2.param = TH2_CHAR;     // Thread Two Attributes
    struct_thread2.count = TH2_COUNT;

    // Create threads with his routine and args.
    // In this case the args is a struct (TThread)
    pthread_create( &thread1, NULL, &print_c, &struct_thread1 );
    pthread_create( &thread2, NULL, &print_c, &struct_thread2 );

    // Waits to terminate thread execution
    // The second argument is the return value
    pthread_join( thread1, NULL );
    pthread_join( thread2, NULL );

    printf ( "Threads Finished !\n\nThread 1 ID: %li\nThread 2 ID: %li\n\n",
            struct_thread1.th_id,
            struct_thread2.th_id );

    return EXIT_SUCCESS;
}
