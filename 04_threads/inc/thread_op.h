#ifndef __THREAD_OP_H__
#define __THREAD_OP_H__
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>


#define DELAY     0x10000
#define TH1_COUNT 0x70
#define TH1_CHAR  'x'

#define TH2_COUNT 0x60
#define TH2_CHAR  'o'


struct TThread
{
    char      param;
    int       count;
    pthread_t th_id;
};



#ifdef __cplusplus
extern "C" {
#endif

void* print_c (void*);

#ifdef __cplusplus
}
#endif

#endif
