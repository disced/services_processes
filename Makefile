TARGET  := run
BIN_DIR := ../bin
INC_DIR := ../inc
OBJ_DIR := ../obj
SRC_DIR := .

CC       = g++
CFLAGS   = -Wall -I$(INC_DIR)
DEBUG    = -g

SRC      := $(wildcard *.cpp)
OBJ      := $(addprefix $(OBJ_DIR)/, $(patsubst %.cpp,%.o, $(wildcard *.cpp)))

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEBUG) -o $(BIN_DIR)/$@  $^

$(OBJ): $(SRC)
	@mkdir -p $(BIN_DIR) $(OBJ_DIR)
	$(CC) $(CFLAGS) $(DEBUG) -c $(notdir $(patsubst %.o,%.cpp, $@)) -o  $@

.PHONY : clean

clean:
	@$(RM) -r $(OBJ_DIR) $(BIN_DIR)
	@echo "Limpio!"
