/*
 * =====================================================================================
 *       Filename:  files.cpp
 *    Description:
 *
 *        Created:  29/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#include <stdlib.h>
#include <unistd.h>
#include "files.h"

int
create_file ( const char *user_file ) {

    char filename[] = "./tmp_XXXXXX";
    int file_descriptor = mkstemp ( filename );

    return file_descriptor;
}

int
write_file ( int file_d, const char *buff, size_t size ) {

    // write ( file_d, &size, sizeof(size) );  // Write bytes to the file.
    // ^-- Se usa por ejemplo para conexiones SSH y que el cliente sepa cuantos bytes de información va a recibir.
    write ( file_d, buff, size );           // Write buffer string in the file.

    return file_d;
}

