/*
 * =====================================================================================
 *       Filename:  interface.cpp
 *    Description:
 *
 *        Created:  28/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include "interface.h"
#include "files.h"

const char *progName;

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
usage ( FILE *fileOutput ) {
    int exitCode;
    fprintf ( fileOutput, "\n\
            Use like this: $ %s -Short Form  \n\
            disced@myhost $  %s -h           \n\n\n\
            Short Form   |    Purpose        \n\
            -------------------------        \n\
            -h           |    Help           \n\
            -c           |    Create File\n\
            -d           |    Delete File\n\
            ",
            progName,
            progName );


    fileOutput == stderr ? exitCode = 1: exitCode = 0;
    exit (exitCode);
}

void
mainLoop ( int argc, char *argv[], const char *shortOpt, const char *new_text ) {

    // Return in optChar the argument char, example: $ ./main -hml
    // optChar has a 'h', next has a 'm' and the last has a 'l'.
    unsigned optChar;

    opterr = 0;   // getopt doesn't print any error messages and return '?' character.
    progName = argv[0];

    // File Variables
    unsigned tmp_file;

    while ( (optChar = getopt(argc, argv, shortOpt) ) != -1 ) {
        switch ( optChar ) {
            case 'h':
                printf("Help:\n\n");
                usage (stdout);
                break;

            case 'c':
                printf("Create File\n\n");
                tmp_file = create_file( "Temporal" );
                write_file ( tmp_file, new_text, strlen(new_text) );
                break;

            case 'd':
                printf("Delete File\n\n");
                if ( tmp_file == 0 )
                    //unlink (new_text);
                break;

            case '?':
                usage (stderr);
                break;

            default:
                usage (stdout);
                break;
        }

    }
}
