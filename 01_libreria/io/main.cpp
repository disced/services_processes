#include "out.h"
#include "calc.h"
#include <stdio.h>
#include <stdlib.h>

int
main ( int argc, char *argv[] ) {

    banner ( " ' < < <  Libs/Calcs  > > > ' " );

    int n1 = 10;
    int n2 = 22;

    print_operation ( n1, n2, sum(n1, n2), '+');
    print_operation ( n1, n2, sub(n1, n2), '-');
    print_operation ( n1, n2, mul(n1, n2), '*');
    print_operation ( n1, n2, divi(n1, n2), '/');
    print_operation ( n1, n2, mod(n1, n2), '%');

    return EXIT_SUCCESS;
}
