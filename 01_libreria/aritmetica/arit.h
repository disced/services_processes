/*
 * =====================================================================================
 *       Filename:  arit.h
 *    Description:
 *
 *        Created:  23/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#ifndef __ARIT_H__
#define __ARIT_H__

#ifdef __cplusplus
extern "C"
{
#endif
    double add  (double, double);
    double sub  (double, double);
    double mult (double, double);
    double divi  (double, double);
    int mod     (int, int);
#ifdef __cplusplus
 }
#endif

#endif
