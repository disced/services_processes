# Programación Concurrente

## Definición

Este metodo de programar es aquel que permite ejecutar más de una
tarea a la vez.

Esto se consigue gracias a los **nucleos** (core) de los procesadores.


## Procesos, Hilos & Demonio (servicio)

Un **proceso** es una instancia de un programa que se está ejecutando,
el proceso se encuentra **aislado** en su propio entorno de ejecución,
con sus hilos correspondientes.

Un **hilo** es una parte del programa que se ejecuta en un nucleo,
se pueden ejecutar varios hilos a la vez en distintos nucleos.

Los **hilos** tienen la posibilidad de **compartir variables**, los **procesos no**.


Un **demonio** *(o servicio en Windows)* es un programa que se ejecuta en segundo plano,
es decir que no le es necesario una entrada o salida de datos *(el teclado, el monitor, los
altavoces)*

## Paradigma

**Paralelismo** ejecución en el mismo intervalo de tiempo.


#### Secuencial

```mermaid

   gantt

  dateFormat HH
  axisFormat %T. Ejecución

  Title Paralelismo Secuencial

  section Ejecución<br/>2 procesos
  PROCESO 1   :s1, 15, 3h
  PROCESO 2   :after s1, 18, 3h

```

#### Real

```mermaid

   gantt

  dateFormat HH
  axisFormat %T. Ejecución

  Title Paralelismo Real

  section Ejecución<br/>2 procesos
  PROCESO 1   :r1, 15, 3h
  PROCESO 2   :r2, 15, 6h

```

#### Simulado

```mermaid

  gantt

  dateFormat HH
  axisFormat %T. Ejecución

  Title Paralelismo Simulado

  section Ejecución<br/>2 procesos
  PROCESO 1   :s1, 15, 1h
  PROCESO 2   :after s1, 16, 1h
  PROCESO 1   :s1, 17, 2h
  PROCESO 2   :after s1, 19, 1h
  PROCESO 1   :s1, 20, 1h

```

**Solapamiento** ejecución en intervalos superpuestos.

**Simultaneidad** ejecución en el mismo instante, no es posible.

## Gestión

La parte encargada de gestionar los procesos es el **Sisitema de Soporte en Tiempo de Ejecución**
(SSTE), que forma parte del SO.


En la gestión de procesos debe existir

1. **Planificación:** Es la asignación de los procesos a los procesadores.
1. **Despacho:** Es la entrega de la CPU al proceso que se ha asignado y que este se ejecute.

### SSTE

El **Sistema de Soporte en Tiempo de Ejecución** se activa cuando:

1. El proceso **termina**
1. El proceso se **bloquea**
1. Finaliza la **cuota de tiempo**
1. Se produce un **error de ejecución**
1. Un proceso con **mayor prioridad** pasa a 'preparado'


El **SSTE** determina la **planificación de un proceso** de distintas maneras:

1. Proceso que **más ha esperado**
1. Proceso **más corto**
1. Por **prioridad**
1. De forma **aleatoria**
1. **Otras** maneras


En la Planificación debe existir **Justicia**. Un planificador es justo
cuando a todo proceso se le asigna en algúm momento un procesador.

Hay que tener en cuenta que no se sabrá cuando se le va a asignar el
procesador a `X` proceso por este motivo, **un programa debe ser independiente
del Planificador.**


### Estados de Procesos

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggTFJcbnN1YmdyYXBoIEVzdGFkb3MgZGUgdW4gUHJvY2Vvc29cbkFbUFJFUEFSQURPXVxuQltFSkVDVUNJw5NOXVxuQ1tCTE9RVUVBRE9dXG5BICYgQiAtLi0-IENcbkEgLS0tLS0tPiBCXG5CIC0tLS0tLT4gQVxuZW5kIiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQiLCJ0aGVtZVZhcmlhYmxlcyI6eyJiYWNrZ3JvdW5kIjoid2hpdGUiLCJwcmltYXJ5Q29sb3IiOiIjRUNFQ0ZGIiwic2Vjb25kYXJ5Q29sb3IiOiIjZmZmZmRlIiwidGVydGlhcnlDb2xvciI6ImhzbCg4MCwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwicHJpbWFyeUJvcmRlckNvbG9yIjoiaHNsKDI0MCwgNjAlLCA4Ni4yNzQ1MDk4MDM5JSkiLCJzZWNvbmRhcnlCb3JkZXJDb2xvciI6ImhzbCg2MCwgNjAlLCA4My41Mjk0MTE3NjQ3JSkiLCJ0ZXJ0aWFyeUJvcmRlckNvbG9yIjoiaHNsKDgwLCA2MCUsIDg2LjI3NDUwOTgwMzklKSIsInByaW1hcnlUZXh0Q29sb3IiOiIjMTMxMzAwIiwic2Vjb25kYXJ5VGV4dENvbG9yIjoiIzAwMDAyMSIsInRlcnRpYXJ5VGV4dENvbG9yIjoicmdiKDkuNTAwMDAwMDAwMSwgOS41MDAwMDAwMDAxLCA5LjUwMDAwMDAwMDEpIiwibGluZUNvbG9yIjoiIzMzMzMzMyIsInRleHRDb2xvciI6IiMzMzMiLCJtYWluQmtnIjoiI0VDRUNGRiIsInNlY29uZEJrZyI6IiNmZmZmZGUiLCJib3JkZXIxIjoiIzkzNzBEQiIsImJvcmRlcjIiOiIjYWFhYTMzIiwiYXJyb3doZWFkQ29sb3IiOiIjMzMzMzMzIiwiZm9udEZhbWlseSI6IlwidHJlYnVjaGV0IG1zXCIsIHZlcmRhbmEsIGFyaWFsIiwiZm9udFNpemUiOiIxNnB4IiwibGFiZWxCYWNrZ3JvdW5kIjoiI2U4ZThlOCIsIm5vZGVCa2ciOiIjRUNFQ0ZGIiwibm9kZUJvcmRlciI6IiM5MzcwREIiLCJjbHVzdGVyQmtnIjoiI2ZmZmZkZSIsImNsdXN0ZXJCb3JkZXIiOiIjYWFhYTMzIiwiZGVmYXVsdExpbmtDb2xvciI6IiMzMzMzMzMiLCJ0aXRsZUNvbG9yIjoiIzMzMyIsImVkZ2VMYWJlbEJhY2tncm91bmQiOiIjZThlOGU4IiwiYWN0b3JCb3JkZXIiOiJoc2woMjU5LjYyNjE2ODIyNDMsIDU5Ljc3NjUzNjMxMjglLCA4Ny45MDE5NjA3ODQzJSkiLCJhY3RvckJrZyI6IiNFQ0VDRkYiLCJhY3RvclRleHRDb2xvciI6ImJsYWNrIiwiYWN0b3JMaW5lQ29sb3IiOiJncmV5Iiwic2lnbmFsQ29sb3IiOiIjMzMzIiwic2lnbmFsVGV4dENvbG9yIjoiIzMzMyIsImxhYmVsQm94QmtnQ29sb3IiOiIjRUNFQ0ZGIiwibGFiZWxCb3hCb3JkZXJDb2xvciI6ImhzbCgyNTkuNjI2MTY4MjI0MywgNTkuNzc2NTM2MzEyOCUsIDg3LjkwMTk2MDc4NDMlKSIsImxhYmVsVGV4dENvbG9yIjoiYmxhY2siLCJsb29wVGV4dENvbG9yIjoiYmxhY2siLCJub3RlQm9yZGVyQ29sb3IiOiIjYWFhYTMzIiwibm90ZUJrZ0NvbG9yIjoiI2ZmZjVhZCIsIm5vdGVUZXh0Q29sb3IiOiJibGFjayIsImFjdGl2YXRpb25Cb3JkZXJDb2xvciI6IiM2NjYiLCJhY3RpdmF0aW9uQmtnQ29sb3IiOiIjZjRmNGY0Iiwic2VxdWVuY2VOdW1iZXJDb2xvciI6IndoaXRlIiwic2VjdGlvbkJrZ0NvbG9yIjoicmdiYSgxMDIsIDEwMiwgMjU1LCAwLjQ5KSIsImFsdFNlY3Rpb25Ca2dDb2xvciI6IndoaXRlIiwic2VjdGlvbkJrZ0NvbG9yMiI6IiNmZmY0MDAiLCJ0YXNrQm9yZGVyQ29sb3IiOiIjNTM0ZmJjIiwidGFza0JrZ0NvbG9yIjoiIzhhOTBkZCIsInRhc2tUZXh0TGlnaHRDb2xvciI6IndoaXRlIiwidGFza1RleHRDb2xvciI6IndoaXRlIiwidGFza1RleHREYXJrQ29sb3IiOiJibGFjayIsInRhc2tUZXh0T3V0c2lkZUNvbG9yIjoiYmxhY2siLCJ0YXNrVGV4dENsaWNrYWJsZUNvbG9yIjoiIzAwMzE2MyIsImFjdGl2ZVRhc2tCb3JkZXJDb2xvciI6IiM1MzRmYmMiLCJhY3RpdmVUYXNrQmtnQ29sb3IiOiIjYmZjN2ZmIiwiZ3JpZENvbG9yIjoibGlnaHRncmV5IiwiZG9uZVRhc2tCa2dDb2xvciI6ImxpZ2h0Z3JleSIsImRvbmVUYXNrQm9yZGVyQ29sb3IiOiJncmV5IiwiY3JpdEJvcmRlckNvbG9yIjoiI2ZmODg4OCIsImNyaXRCa2dDb2xvciI6InJlZCIsInRvZGF5TGluZUNvbG9yIjoicmVkIiwibGFiZWxDb2xvciI6ImJsYWNrIiwiZXJyb3JCa2dDb2xvciI6IiM1NTIyMjIiLCJlcnJvclRleHRDb2xvciI6IiM1NTIyMjIiLCJjbGFzc1RleHQiOiIjMTMxMzAwIiwiZmlsbFR5cGUwIjoiI0VDRUNGRiIsImZpbGxUeXBlMSI6IiNmZmZmZGUiLCJmaWxsVHlwZTIiOiJoc2woMzA0LCAxMDAlLCA5Ni4yNzQ1MDk4MDM5JSkiLCJmaWxsVHlwZTMiOiJoc2woMTI0LCAxMDAlLCA5My41Mjk0MTE3NjQ3JSkiLCJmaWxsVHlwZTQiOiJoc2woMTc2LCAxMDAlLCA5Ni4yNzQ1MDk4MDM5JSkiLCJmaWxsVHlwZTUiOiJoc2woLTQsIDEwMCUsIDkzLjUyOTQxMTc2NDclKSIsImZpbGxUeXBlNiI6ImhzbCg4LCAxMDAlLCA5Ni4yNzQ1MDk4MDM5JSkiLCJmaWxsVHlwZTciOiJoc2woMTg4LCAxMDAlLCA5My41Mjk0MTE3NjQ3JSkifX0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggTFJcbnN1YmdyYXBoIEVzdGFkb3MgZGUgdW4gUHJvY2Vvc29cbkFbUFJFUEFSQURPXVxuQltFSkVDVUNJw5NOXVxuQ1tCTE9RVUVBRE9dXG5BICYgQiAtLi0-IENcbkEgLS0tLS0tPiBCXG5CIC0tLS0tLT4gQVxuZW5kIiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQiLCJ0aGVtZVZhcmlhYmxlcyI6eyJiYWNrZ3JvdW5kIjoid2hpdGUiLCJwcmltYXJ5Q29sb3IiOiIjRUNFQ0ZGIiwic2Vjb25kYXJ5Q29sb3IiOiIjZmZmZmRlIiwidGVydGlhcnlDb2xvciI6ImhzbCg4MCwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwicHJpbWFyeUJvcmRlckNvbG9yIjoiaHNsKDI0MCwgNjAlLCA4Ni4yNzQ1MDk4MDM5JSkiLCJzZWNvbmRhcnlCb3JkZXJDb2xvciI6ImhzbCg2MCwgNjAlLCA4My41Mjk0MTE3NjQ3JSkiLCJ0ZXJ0aWFyeUJvcmRlckNvbG9yIjoiaHNsKDgwLCA2MCUsIDg2LjI3NDUwOTgwMzklKSIsInByaW1hcnlUZXh0Q29sb3IiOiIjMTMxMzAwIiwic2Vjb25kYXJ5VGV4dENvbG9yIjoiIzAwMDAyMSIsInRlcnRpYXJ5VGV4dENvbG9yIjoicmdiKDkuNTAwMDAwMDAwMSwgOS41MDAwMDAwMDAxLCA5LjUwMDAwMDAwMDEpIiwibGluZUNvbG9yIjoiIzMzMzMzMyIsInRleHRDb2xvciI6IiMzMzMiLCJtYWluQmtnIjoiI0VDRUNGRiIsInNlY29uZEJrZyI6IiNmZmZmZGUiLCJib3JkZXIxIjoiIzkzNzBEQiIsImJvcmRlcjIiOiIjYWFhYTMzIiwiYXJyb3doZWFkQ29sb3IiOiIjMzMzMzMzIiwiZm9udEZhbWlseSI6IlwidHJlYnVjaGV0IG1zXCIsIHZlcmRhbmEsIGFyaWFsIiwiZm9udFNpemUiOiIxNnB4IiwibGFiZWxCYWNrZ3JvdW5kIjoiI2U4ZThlOCIsIm5vZGVCa2ciOiIjRUNFQ0ZGIiwibm9kZUJvcmRlciI6IiM5MzcwREIiLCJjbHVzdGVyQmtnIjoiI2ZmZmZkZSIsImNsdXN0ZXJCb3JkZXIiOiIjYWFhYTMzIiwiZGVmYXVsdExpbmtDb2xvciI6IiMzMzMzMzMiLCJ0aXRsZUNvbG9yIjoiIzMzMyIsImVkZ2VMYWJlbEJhY2tncm91bmQiOiIjZThlOGU4IiwiYWN0b3JCb3JkZXIiOiJoc2woMjU5LjYyNjE2ODIyNDMsIDU5Ljc3NjUzNjMxMjglLCA4Ny45MDE5NjA3ODQzJSkiLCJhY3RvckJrZyI6IiNFQ0VDRkYiLCJhY3RvclRleHRDb2xvciI6ImJsYWNrIiwiYWN0b3JMaW5lQ29sb3IiOiJncmV5Iiwic2lnbmFsQ29sb3IiOiIjMzMzIiwic2lnbmFsVGV4dENvbG9yIjoiIzMzMyIsImxhYmVsQm94QmtnQ29sb3IiOiIjRUNFQ0ZGIiwibGFiZWxCb3hCb3JkZXJDb2xvciI6ImhzbCgyNTkuNjI2MTY4MjI0MywgNTkuNzc2NTM2MzEyOCUsIDg3LjkwMTk2MDc4NDMlKSIsImxhYmVsVGV4dENvbG9yIjoiYmxhY2siLCJsb29wVGV4dENvbG9yIjoiYmxhY2siLCJub3RlQm9yZGVyQ29sb3IiOiIjYWFhYTMzIiwibm90ZUJrZ0NvbG9yIjoiI2ZmZjVhZCIsIm5vdGVUZXh0Q29sb3IiOiJibGFjayIsImFjdGl2YXRpb25Cb3JkZXJDb2xvciI6IiM2NjYiLCJhY3RpdmF0aW9uQmtnQ29sb3IiOiIjZjRmNGY0Iiwic2VxdWVuY2VOdW1iZXJDb2xvciI6IndoaXRlIiwic2VjdGlvbkJrZ0NvbG9yIjoicmdiYSgxMDIsIDEwMiwgMjU1LCAwLjQ5KSIsImFsdFNlY3Rpb25Ca2dDb2xvciI6IndoaXRlIiwic2VjdGlvbkJrZ0NvbG9yMiI6IiNmZmY0MDAiLCJ0YXNrQm9yZGVyQ29sb3IiOiIjNTM0ZmJjIiwidGFza0JrZ0NvbG9yIjoiIzhhOTBkZCIsInRhc2tUZXh0TGlnaHRDb2xvciI6IndoaXRlIiwidGFza1RleHRDb2xvciI6IndoaXRlIiwidGFza1RleHREYXJrQ29sb3IiOiJibGFjayIsInRhc2tUZXh0T3V0c2lkZUNvbG9yIjoiYmxhY2siLCJ0YXNrVGV4dENsaWNrYWJsZUNvbG9yIjoiIzAwMzE2MyIsImFjdGl2ZVRhc2tCb3JkZXJDb2xvciI6IiM1MzRmYmMiLCJhY3RpdmVUYXNrQmtnQ29sb3IiOiIjYmZjN2ZmIiwiZ3JpZENvbG9yIjoibGlnaHRncmV5IiwiZG9uZVRhc2tCa2dDb2xvciI6ImxpZ2h0Z3JleSIsImRvbmVUYXNrQm9yZGVyQ29sb3IiOiJncmV5IiwiY3JpdEJvcmRlckNvbG9yIjoiI2ZmODg4OCIsImNyaXRCa2dDb2xvciI6InJlZCIsInRvZGF5TGluZUNvbG9yIjoicmVkIiwibGFiZWxDb2xvciI6ImJsYWNrIiwiZXJyb3JCa2dDb2xvciI6IiM1NTIyMjIiLCJlcnJvclRleHRDb2xvciI6IiM1NTIyMjIiLCJjbGFzc1RleHQiOiIjMTMxMzAwIiwiZmlsbFR5cGUwIjoiI0VDRUNGRiIsImZpbGxUeXBlMSI6IiNmZmZmZGUiLCJmaWxsVHlwZTIiOiJoc2woMzA0LCAxMDAlLCA5Ni4yNzQ1MDk4MDM5JSkiLCJmaWxsVHlwZTMiOiJoc2woMTI0LCAxMDAlLCA5My41Mjk0MTE3NjQ3JSkiLCJmaWxsVHlwZTQiOiJoc2woMTc2LCAxMDAlLCA5Ni4yNzQ1MDk4MDM5JSkiLCJmaWxsVHlwZTUiOiJoc2woLTQsIDEwMCUsIDkzLjUyOTQxMTc2NDclKSIsImZpbGxUeXBlNiI6ImhzbCg4LCAxMDAlLCA5Ni4yNzQ1MDk4MDM5JSkiLCJmaWxsVHlwZTciOiJoc2woMTg4LCAxMDAlLCA5My41Mjk0MTE3NjQ3JSkifX0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)


### Realciones de Procesos

1. **Independencia**: procesos independientes unos de otros.
1. **Competencia**:   compiten por recursos escasos *(RAM, Microprocesador...)*.
1. **Cooperación**:   trabajan en diferentes partes de un problema para llegar a una solución comúm.


## Instrucciones

A la hora de ejecutar un programa con varios hilos de ejecución que acceden a un 
recurso en común como por ejemplo un contador:
```c++
int counter = 0;
```
uno de los hilos sumará y el otro restará, el problema es donde el **SSTE** para la 
ejecución de cada hilo para dejar al otro hilo seguir/acceder a su sección critica.

El hilo que aumenta el contador le suma 100 y el otro le resta 100, la logica indica
que el contador se queda a 0, pero no es así ya que en ocasiones se para la ejecución
en **Instrucciones No Atomicas**

Existen dos tipos de **Instrucciones Atomicas**

1. **Grano Fino**: Son operaciones de hardware, por ejemplo `int x = 10;`, este tipo de
operación no se puede divir su ejecución ya que a bajo nivel ejecuta varias instrucciones sobre
el hardware real.

2. **Grano Grueso**: Son operaciones de software que el **SSTE** puede dividir pero el programador
no se lo permite, con diferentes herramientas.

## Procesos en `C`

| Función/Tipo | Definición |
| ------- | ---------- |
| `fork()`     | Crea un proceso hijo, devuelve un `>0` en el padre y `0` en el hijo |
| `get_pid()`  | Devuelve el PID del proceso  |
| `get_ppid()` | Devuelve el PPID del proceso |
| `wait()`     | El padre espera a que el hijo termine, se le pasa por referencia un `int` para guardar el valor de `exit()` del hijo|
| `execvp()`   | Funcion exec sin el path completo y con lista de argumentos terminada en `NULL` |
| `exec`       | Cambia el proceso actual por otro *(Firefox)* y cuando termina no vuelve al anterior |
| `pid_t`      | Es un `typedef` de un `int` en estandar `POSIX` |

#### Ejemplo `execvp`

```c++
#include <unistd.h>

char *command          = "ls";
char *commandOptions[] = { "ls", "-lah", NULL };

execvp( command, commandOptions );
printf( "An error has occurred in the exec function \n" );  

/* Exec falla, se ejecuta el printf. 
 * Exec funciona, no se ejectua printf 
 * ya que no vuelve a este proceso.
*/
```

## Signals `C`

El OS nos provee de un manejador de señales, pero también podemos 
crear el nuestro propio para todas las señales menos para `SIGKILL`.

Utilizamos la función `sigaction(SEÑAL, &struct, NULL)` y un `struct sigacion`
el cual lo dejamos a `0` y modificamos su dato: `sa_handler = &funcionHandler; `

```c++
#include <signal.h>
#include <strings.h> // bzero()

void
handler ( int signal_type ) {

  switch ( signal_type ) {
    case SIGUSR1:
        // something...
    break;
    case SIGUSR2:
        // something...
    break;
  }
}

int
main () {
  struct sigaction sa;
  bzero ( &sa, sizeof(sa) ); // Todos los campos del struct a 0 ( hace un memset() )

  sa.sa_handler = &handler;  // Configura el handler
  
  // Manejador de dos señales:
  sigaction( SIGUSR1, &sa, NULL );
  sigaction( SIGUSR2, &sa, NULL );
  
  return 0;
}

```

## Threads

Usamos `pthread.h` ya que es POSIX y compilamos con `-lpthread`

| Función/Tipo | Definición |
| ------------ | ---------- |
| `pthread_t th1` | Declarar una variable de tipo hilo |
| `pthread_create( &th1, NULL, &function, &arguments )` | Crea un hilo *(Análogo a fork())* |
| `pthread_join( th1, NULL )` | Espera a que termine la ejecución del `th1` |

Las **funciones** que los threads van a ejecutra deben devolver `void *` y que su parametro sea 
`void *param`.

Es habitual que el parametro sea una estructura con los datos y dentro de la misma función 
hacemos un `cast` a esta estructura, y cuando llamamos a la función `pthread_create()` le
pasamos por referencia `&laEstructura`, es decir que es una dirección de memoria por eso
el `cast` dentro de la función.

## Mutex

El mutex es como un semáforo a 1, bloquea una sección critica para que solo un hilo pueda acceder
a esa sección.

| Función/Tipo | Definición |
| ------------ | ---------- |
| `pthread_mutext_t myMutex;` | Declaración de una variable del tipo pthread_mutext_t |
| `pthread_mutex_init( &myMutex, NULL)`  | Inicializa el mutex con atributos por omisión |
| `myMutex = PTHREAD_MUTEX_INITIALIZER`  | Lo mismo que `pthread_mutex_init( &myMutex, NULL)` |
| `pthread_mutex_lock( &myMutex )`       | Bloquea el mutex |
| `pthread_mutex_unlock( &myMutex )`     | Ddesbloquea el mutex |


## Semaphore

Los semaforos los provee el sistema operativo y sirven para determinar
cuantos hilos va a acceder a la vez a la sección critica.

Es posible combinarlos con `mutex`.

| Función/Tipo | Definición |
| ------------ | ---------- |
| `sem_t semaphore` | Declaración del semaforo |
| `sem_init( &semaphore, NULL, 2 )` | Inicia el semaforo a 2 sin compartir entre procesos  |
| `sem_wait( &semaphore )` | Disminuye en 1 el valor del semaforo y entra a la sección critica |
| `sem_post( &semaphore )` | Aumenta en 1 el semaforo cuando ha terminado la sección critica |
| `sem_destroy( &semaphore )` | Destruye el semaforo |

## Libreria

### Estatica

Para crear una libreria estatica se usa el comando `ar` a partir de los **objetos** (`.o`)
La nomenclatura de las librerias es la siguiete: `libnombre.a` -> `libsum.a`


```bash
disced@myhost $ ar cr libaritmetica.a basico.o avanzado.o
```

A la hora de linkar la salida del programa debemos especificar cual es la libreria
con el argumento **`-lnombre`** y **`-L/ruta/del/archivo.a`**


```bash
disced@myhost $ g++ -o calculos main.o -laritmetica -L.
```

Con el comando **`nm`** podremos comprobar los nombres de las funciones en ficheros objeto,
y así nos aseguramos si se ha producido **Name Mangling**.

```bash
disced@myhost $ nm basico.o

basico.o:
0000000000000000 T add
000000000000001e T sub




disced@myhost $ nm avanzado.o

avanzado.o
000000000000001e T divi
000000000000003c T mod
0000000000000000 T mult




disced@myhost $ nm libaritmetica.a

avanzado.o:
000000000000001e T divi
000000000000003c T mod
0000000000000000 T mult

basico.o:
0000000000000000 T add
000000000000001e T sub
```

### Dinamicas

Para crear librerias dinamicas (**Shared Object**) debemos compilar los ficheros que deseamos
archivar en un `.so` con el argumento y `-fPIC`, para ello compilamos cada fichero a fichero objeto.

```bash
disced@myhost $ g++ -c -fPIC basico.cpp

disced@myhost $ g++ -c -fPIC avabzado.cpp

```

Con los ficheros objeto vamos a crear el archivo `.so` linkando los mismos e indicamos que
será una libreria compartida mediante el argumento `-shared`.


```bash

disced@myhost $ g++ -shared -o libcalcs.so basico.o avanzado.o

```

Hemos creado un archivo que contiene dos ficheros y es dinamico.


Falta **linkar** el archivo **main.o** con la libreria dinamica.


```bash

disced@myhost $ g++ -o calcula main.o -lcalcs -L.

```

Si ejecutamos `./calcula` no funciona ya que el sistema desconoce la ruta
de la libreria `libcalcs.so`, por este motivo debemos **modifcar** el valor de
la variable de entorno **`$LD_LIBRARY_PATH`**.

```bash

disced@myhost $ export LD_LIBRARY_PATH=/home/disce/work/services_processes/01_libreria/io:$LD_LIBRARY_PATH

```





